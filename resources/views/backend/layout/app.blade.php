<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>{{ config('app.name') }}</title>

    <script>window.Laravel = { csrfToken:'{{csrf_token()}}'}</script>

    <link rel="icon" href="{{ asset('assets/images/logo.png') }}">    
    @include('backend.includes.style')
</head>
<body>
    @yield('content')
</body>    
@include('backend.includes.script')
</html>
